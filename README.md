###Security yaratılır
kubectl create ns keycloak-system

helm upgrade --install --namespace keycloak-system keycloak codecentric/keycloak -f keycloak.yaml

###İstio yaratılır

kubectl create ns istio-system

curl -L https://git.io/getLatestIstio | ISTIO_VERSION=1.5.1 sh -
cd istio-1.5.1

for i in install/kubernetes/helm/istio-init/files/crd*yaml; do kubectl apply -f $i; done



helm template install/kubernetes/helm/istio
		--set global.mtls.enabled=false 
		--set tracing.enabled=true 
		--set kiali.enabled=true 
		--set grafana.enabled=true 
		--namespace istio-system > istio.yaml
		
		
kubectl apply -f istio.yaml

kubectl label namespace backend-system istio-injection=enabled



####Backend yaratılır
kubectl create ns backend-system

helm upgrade --install --namespace backend-system --set image=registry.gitlab.com/kadirroi/products-service:latest --timeout 120s --force products-service ./helm-products-service 

helm upgrade --install --namespace backend-system --set image=registry.gitlab.com/kadirroi/products-service:latest --timeout 120s --force products-service ./helm-products-service  --dry-run --debug

helm upgrade --install --namespace backend-system --timeout 120s --force microservices-gateway ./microservices-gateway

helm upgrade --install --namespace backend-system --set image=registry.gitlab.com/kadirroi/hello-world:latest --timeout 120s --force products-service ./helm-products-service  --dry-run --debug


kubectl apply -f - <<EOF
apiVersion: "authentication.istio.io/v1alpha1"
kind: "Policy"
metadata:
  name: jwt-keycloak
  namespace: istio-system
spec:
  targets:
  - name: istio-ingressgateway
  origins:
  - jwt:
       issuer: "http://35.225.171.123/auth/realms/dev"
      jwksUri: "http://35.225.171.123/auth/realms/dev/protocol/openid-connect/certs"
				http://35.225.171.123/auth/realms/dev/protocol/openid-connect/certs
  principalBinding: USE_ORIGIN
EOF



###frontend yaratılır
kubectl create ns frontend-system