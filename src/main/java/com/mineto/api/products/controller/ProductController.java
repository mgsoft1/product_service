package com.mineto.api.products.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/products-service/v1/products")
public class ProductController {
	@GetMapping("/v1")
	List<String> listProducts() {
		return Arrays.asList("Computer", "MobilePhone");
	}
}
